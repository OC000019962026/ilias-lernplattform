# ILIAS - Lernplattform



# This repository adheres to the publiccode.yml standard by including this 
# metadata file that makes public software easily discoverable.
# More info at https://github.com/italia/publiccode.yml


publiccodeYmlVersion: '0.2'

categories:
  - test-management
  - network-management
  - learning-management-system
  - knowledge-management
  - gamification
  - collaboration

name: Illias
url: >-
  https://docu.ilias.de/ilias.php?baseClass=illmpresentationgui&cmd=layout&ref_id=35&obj_id=154883

releaseDate: '2023-12-12'

developmentStatus: stable

softwareVersion: '7.27'

platforms:
  - windows
  - mac
  - linux

softwareType: standalone/web

maintenance:
  type: community

legal:
  license: GPL-3.0-or-later

localisation:
  localisationReady: true

  availableLanguages:
    - de
    - en

description:
  de:

    genericName: Ilias - Lernplattform
    shortDescription: ILIAS ist eine leistungsfähige Lernplattform (Learning Management System).

    longDescription: >2

       Seit 1998 nutzen Universitäten, Unternehmen, Schulen und der öffentliche Dienst die integrierten Werkzeuge, um individuelle      
       Lernszenarien und Trainings umzusetzen.

      ​


      Die Entwicklung wird von der internationalen ILIAS-Community gesteuert und vom ILIAS-Verein koordiniert. Ein professionelles   
      Netzwerk von Dienstleistern nimmt Anpassungen für Sie vor und bietet Hosting und Schulungen an.


      ​
      Jeder kann ILIAS kostenfrei nutzen und die weitere Entwicklung mitgestalten.


      ​Hochschulen: Als Projekt an der Uni Köln geboren und gewachsen, steht Ilias für flexibles und modernes E-Learning für  
                   Hochschulen.


      ​
      Unternehmen: Open Source im Unternehmen? Kostengünstig und effektiv. Sparen Sie sich teure Experimente und profitieren Sie von den
                   Möglichkeiten, die ILIAS Ihnen bietet.


      ​
      Schulen: Auch ohne eigenen Server können Schulen ILIAS nutzen und sich im ILIAS-Verein mit anderen Schulen austauschen.


      ​
      Öffentlicher Dienst: Onboarding, Aus- und Weiterbildungen auch mit komplexen Anforderungen werden seit Jahren datenschutzkonform  
                           und sicher mit ILIAS umgesetzt.


      ​


      ​Vielseitig

      Kurs-Management, Lernmodule, Tests und Prüfungen, Portfolios, Umfragen,nWikis und Blogs stehen sofort zur Verfügung und machen  
      ILIAS zur idealen E-Learning-Lösung "aus einem Guss".



      Open-Source

      Sie müssen niemals Lizenzgebühren bezahlen. Spezielle Anforderungen können von unseren offiziellen Dienstleistern für Sie 
      umgesetzt werden. Eine freiwillige Mitgliedschaft im ILIAS-Verein ist kostengünstig und erlaubt
      Ihnen, direkt auf den Entwicklungsprozess Einfluss zu nehmen.



      ​Gehört Ihnen

      Ändern Sie das Aussehen, implementieren Sie Ihre Lernszenarien, integrieren Sie es mit Ihrer Website oder Ihrem Intranet. 
      Aktivieren Sie nur die Module, die Sie auch brauchen, und geben Sie jedem Ihrer Benutzer und Benutzerinnen die gewünschten  
      Bearbeitungs- und Nutzungsrechte.


      ​
      Zuverlässig

      Bereits seit 1998 in der Entwicklung, wird es heute von 12 Premium-Partnern und mehr als 40 institutionellen Vereinsmitgliedern
      unterstützt. Weltweit gibt es tausende aktive ILIAS-Installationen mit Millionen von Nutzerinnen und Nutzern.


      ​
      Standardkonform
      
      SCORM 1.2 und SCORM 2004-Lernmodule. LOM Metadaten. IMS QTI Tests und Prüfungen. XML, CSV- und Excel-Exporte Ihrer Daten. IMS LTI 
      zur Einbindung externer Anwendungen. Einmal installiert, läuft ILIAS vollständig in Ihrem
      Internet-Browser. Es wird keine weitere Software benötigt - weder für Lehrerinnen, Lerner oder Hilfskräfte.



    features:
    
      - Lernmodule
      - Tests
      - Prüfungen
      - Portfolios
      - Blogs
      - Wikis
      - Foren
      - Umfragen
